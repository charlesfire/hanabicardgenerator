const fs = require('fs');
const { JSDOM } = require('jsdom');

const dom = new JSDOM(`<!DOCTYPE html><html></html>`);
const window = dom.window;
const document = window.document;

const { SVG, Defs, G, registerWindow } = require('@svgdotjs/svg.js');
const svg2png = require('svg2png');

registerWindow(window, document);

let config = JSON.parse(fs.readFileSync('./config.json'));
let defs = new Defs();

config.solidColors.forEach(solidColor => {
  defs.gradient('linear', add => {
    add.stop(0, solidColor.color);
  }).id(solidColor.id);
});

config.linearGradients.forEach(linearGradient => {
  defs.gradient('linear', add => {
    let numColors = linearGradient.colors.length;
    for (let i = 0; i < numColors; i++) {
      add.stop(i / (numColors - 1), linearGradient.colors[i]);
    }
  }).id(linearGradient.id)
    .from('0%', '0%')
    .to('100%', '100%');
});

config.radialGradients.forEach(radialGradient => {
  defs.gradient('radial', add => {
    let numColors = radialGradient.colors.length;
    for (let i = 0; i < numColors; i++) {
      add.stop(i / (numColors - 1), radialGradient.colors[i]);
    }
  }).id(radialGradient.id)
    .radius('100%');
});

let backgrounds = [];
config.backgrounds.forEach(backgroundConfig => {
  let background = new G();
  background.rect('100%', '100%').fill(backgroundConfig.fill);
  backgrounds[backgroundConfig.name] = background;
});

const draw = SVG().addTo(document.documentElement).size(220, 343);
draw.add(defs);

const card = draw.group();
card.rect('100%', '100%');

const drawArea = card.group();
const drawAreaClipPath = draw.clip();
drawAreaClipPath.rect(200, 323).x(10).y(10);
drawArea.clipWith(drawAreaClipPath);

for (suit of config.suits) {
  for (backgroundName of suit.backgrounds) {
    for (cardConfig of suit.cards) {
      drawArea.add(backgrounds[backgroundName]);
      
      fs.writeFileSync('./out/' + backgroundName + '_' + suit.name + '_' + cardConfig.character + '.png', svg2png.sync(draw.svg()));
      drawArea.clear();
    }
  }
}
